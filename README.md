# Система нарядов НОРДКОМП

## установка пакетов 
```
yarn install
```

## Запуск в режиме разработки
``` shell
yarn start
```

## Прод. сборка
``` 
yarn build
```

## API
[./docs/swagger.yaml](./docs/swagger.yaml)

### Запуск локального Swagger UI
```
yarn swagger
```