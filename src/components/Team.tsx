import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt as faPencil } from "@fortawesome/free-solid-svg-icons";
import Button from "components/common/Button";

import { ITeam } from "types/Team";

import styles from "assets/styles/components/Team.module.scss";

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  data: ITeam;
}

function Team({ className, data, ...rest }: IProps) {
  const { title, leader, users } = data;
  return (
    <div className={[styles.wrap, className].join(" ")} {...rest}>
      <div className={styles.title}>
        <div className={styles.dt}>Действующая бригада:</div>
        <div className={styles.dd}>{title}</div>
      </div>

      <div className={styles.team}>
        <div className={styles.dt}>Бригадир:</div>
        <div>
          <div className={styles.dd}>
            {leader.firstName} {leader.lastName}
          </div>
          {users.map(({ firstName, lastName }, key) => (
            <div className={styles.dd} key={key}>
              {firstName} {lastName}
            </div>
          ))}
        </div>
      </div>

      <div className={styles.button}>
        <Button href="/teams" type="small">
          <FontAwesomeIcon icon={faPencil} />
          Изменить бригаду
        </Button>
      </div>
    </div>
  );
}

export default Team;
