import React, { HTMLAttributes } from "react";
import { Link } from "react-router-dom";

import styles from "assets/styles/components/common/Button.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  type?: "basic" | "small" | "invert" | "simple" | "borderless";
  action?: Function;
  href?: string;
  disabled?: boolean;
}

const Button: React.FC<Props> = ({
  className,
  children,
  href,
  action,
  type = "basic",
  disabled = false,
}) => {
  const style =
    type !== "simple"
      ? [
          styles.wrap,
          styles[type],
          disabled ? styles.disabled : "",
          className,
        ].join(" ")
      : className;
  return href ? (
    <Link to={disabled ? "" : href} className={style}>
      {children}
    </Link>
  ) : (
    <div
      className={style}
      onClick={(e: any) => (!disabled && action ? action(e) : {})}
    >
      {children}
    </div>
  );
};

export default Button;
