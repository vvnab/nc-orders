import React from "react";

import styles from "assets/styles/components/common/RadioButton.module.scss";

interface IProps extends React.HTMLProps<HTMLInputElement> {
  label: string;
  errored?: boolean;
  isRequired?: boolean;
}

const RadioButton = ({
  // checked,
  label,
  errored,
  isRequired,
  className,
  ...rest
}: IProps) => (
  <div className={[styles.wrap, className].join(" ")}>
    <label>
      <input type="radio" {...rest} />
      <div className={styles.checkmark}></div>
      {label} {isRequired && <span className={styles.asterisk}>&#42;</span>}
    </label>
  </div>
);

export default RadioButton;
