import React, { useState, useRef, useEffect } from "react";
import useWindowSize from "hooks/useWindowSize";
import chevronDownIcon from "assets/icons/chevron.svg";
import { ISelect } from "types";

import styles from "assets/styles/components/common/Select.module.scss";

interface IProps {
  label?: string;
  errored?: boolean;
  isRequired?: boolean;
  values?: ISelect[];
  selected?: string;
  className?: string;
  placeholder?: string;
  onChange?: Function;
}

const Select = ({
  values = [],
  className,
  placeholder,
  onChange,
  selected,
}: IProps) => {
  const { width } = useWindowSize();
  const isMobile = width < 1024;
  const el = useRef(null);
  let selectedItem: number | undefined = values.findIndex(
    (i) => i.value === selected
  );
  selectedItem = selectedItem >= 0 ? selectedItem : undefined;
  const [subMenu, setSubMenu] = useState(false);

  const onClick = (e: React.MouseEvent) => {
    if (values && values.length > 0) {
      setSubMenu(!subMenu);
    }
  };

  const handleClickOutside = (e: MouseEvent) => {
    // @ts-ignore
    if (el && el.current && el.current.contains(e.target)) {
      return;
    } else {
      setSubMenu(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside, false);
    return () =>
      document.removeEventListener("mousedown", handleClickOutside, false);
  }, []);
  return isMobile ? (
    <div className={styles.wrap}>
      <select
        className={[styles.select, className].join(" ")}
        onChange={(e) => {
          onChange && onChange(e.currentTarget.value);
        }}
        defaultValue={selected || placeholder}
      >
        {placeholder && <option disabled>{placeholder}</option>}
        {values.map(({ title, value }, key) => (
          <option value={value} key={key}>
            {title}
          </option>
        ))}
      </select>
      <div className={styles.down}>
        <img src={chevronDownIcon} alt="" />
      </div>
    </div>
  ) : (
    <div className={styles.wrap} ref={el}>
      <div
        className={[
          styles.select,
          className,
          subMenu ? styles.opened : "",
        ].join(" ")}
        onClick={onClick}
      >
        {placeholder && selectedItem === undefined
          ? placeholder
          : values[selectedItem || 0].title}
      </div>
      <div className={styles.down}>
        <img src={chevronDownIcon} alt="" />
      </div>

      {subMenu && (
        <div className={styles.subMenuContainer}>
          <div className={styles.subMenu}>
            {values.map(({ title, value }, k) => (
              <div
                className={[
                  styles.subMenuItem,
                  selectedItem === k && styles.selected,
                ].join(" ")}
                key={`${k}-${value}`}
                onClick={(e) => {
                  // selectItem(k);
                  setSubMenu(false);
                  onChange && onChange(value);
                }}
              >
                {title}
              </div>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

export default Select;
