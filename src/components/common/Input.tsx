import React from "react";

import styles from "assets/styles/components/common/Input.module.scss";

interface IProps extends React.HTMLProps<HTMLInputElement> {
  className?: string;
  errored?: boolean;
}

const Select = ({ className, errored, ...rest }: IProps) => {
  return (
    <input
      className={[styles.wrap, errored ? styles.errored : "", className].join(
        " "
      )}
      {...rest}
      autoComplete="off"
    />
  );
};

export default Select;
