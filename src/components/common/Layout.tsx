import React from "react";

import styles from "assets/styles/components/common/Layout.module.scss";

interface IProps extends React.HTMLProps<HTMLDivElement> {}

function Layout({ className, children, ...rest }: IProps) {
  return (
    <div className={[styles.wrap, className].join(" ")} {...rest}>
      {children}
    </div>
  );
}

export default Layout;
