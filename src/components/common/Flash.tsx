import React, { HTMLAttributes, useEffect } from "react";

import styles from "assets/styles/components/common/Flash.module.scss";

interface Props extends HTMLAttributes<HTMLDivElement> {
  message?: string;
  close?: Function;
  type?: "success" | "error";
  timeout?: number;
}

const Flash: React.FC<Props> = ({
  className,
  message,
  close,
  type,
  timeout = 4000,
}) => {
  useEffect(() => {
    console[type === "error" ? "error" : "info"](message);
    const handler = setTimeout(() => close && close(), timeout);
    return () => clearTimeout(handler);
  }, [close, timeout, message, type]);

  return (
    <div className={styles.wrap}>
      <div
        className={[
          styles.flash,
          type === "success"
            ? styles.success
            : type === "error"
            ? styles.error
            : "",
          className,
        ].join(" ")}
        onClick={() => close && close()}
      >
        {message}
      </div>
    </div>
  );
};

export default Flash;
