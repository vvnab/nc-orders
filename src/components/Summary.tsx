import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt as faCalendar } from "@fortawesome/free-solid-svg-icons";
import Button from "components/common/Button";
import { DateTime } from "luxon";

import styles from "assets/styles/components/Summary.module.scss";

interface IProps extends React.HTMLProps<HTMLDivElement> {
  data: any;
}

function Summary({ className, data, ...rest }: IProps) {
  return (
    <div className={[styles.wrap, className].join(" ")} {...rest}>
      <div className={styles.title}>Результаты работы бригады:</div>

      <table className={styles.table}>
        <tbody>
          <tr>
            <th />
            {data.dates.map((i: string) => {
              const date = DateTime.fromISO(i);
              return <th key={i}>{`${date.day} ${date.monthShort}`}</th>;
            })}
            <th>Итого:</th>
          </tr>
          {data.rows.map(
            ({ title, values }: { title: string; values: any[] }) => (
              <tr key={title}>
                <th>{title}</th>
                {values.map((i, key) => (
                  <td key={key}>{i || "---"}</td>
                ))}
                <td>
                  {values.reduce((s, i) => {
                    if (i) {
                      return (s || 0) + i;
                    } else {
                      return s;
                    }
                  }, undefined) || "---"}
                </td>
              </tr>
            )
          )}
        </tbody>
      </table>

      <div className={styles.button}>
        <Button href="/reports" type="small">
          <FontAwesomeIcon icon={faCalendar} />
          Подробнее за период
        </Button>
      </div>
    </div>
  );
}

export default Summary;
