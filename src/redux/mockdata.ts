import { ITeam, IUser } from "types";

export const summary = {
  dates: ["2021-05-11", "2021-05-12", "2021-05-13"],
  rows: [
    {
      title: "t таб. (ч)",
      values: [12, 15, 1],
    },
    {
      title: "t нар.откр (нч)",
      values: [12, 115, 0],
    },
    {
      title: "DF/ASD",
      values: [12, 65, 54],
    },
    {
      title: "Выработка",
      values: [12, 10, 12],
    },
    {
      title: "JHYghJJ",
      values: [32, 15, 17],
    },
    {
      title: "POO/HJJ sd",
      values: [12, null, null],
    },
    {
      title: "ЗП с выраб.",
      values: [undefined, null, null],
    },
    {
      title: "POIOU",
      values: [undefined, null, undefined],
    },
  ],
};

export const users: IUser[] = [
  {
    id: 1,
    username: "vvnab",
    firstName: "Артур Викторович",
    lastName: "Петров",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 2,
    username: "abobab",
    firstName: "Иван Алексеевич",
    lastName: "Горбунов",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 3,
    username: "abod",
    firstName: "Олег Сегргеевич",
    lastName: "Ложкин",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 4,
    username: "abo34d",
    firstName: "Тимур Игоревич",
    lastName: "Долгонос",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 5,
    username: "asdrbod",
    firstName: "Емельян Вениаминович",
    lastName: "Перельман",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 6,
    username: "a234b4od",
    firstName: "Егор Николаевич",
    lastName: "Бозе",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 7,
    username: "abo890d",
    firstName: "Денис Сергеевич",
    lastName: "Карамзин",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 8,
    username: "ab97084eod",
    firstName: "Леонид Павлович",
    lastName: "Пукин",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 9,
    username: "abxsdg5od",
    firstName: "Марат Анатольевич",
    lastName: "Харитонов",
    email: "",
    phone: "",
    position: "",
  },
  {
    id: 10,
    username: "a1b32od",
    firstName: "Елисей Аркадьевич",
    lastName: "Полищук",
    email: "",
    phone: "",
    position: "",
  },
];

const [leader, ...teamUsers] = users;

export const team: ITeam = {
  id: 1,
  title: "ППСС",
  leader,
  users: teamUsers,
};

export const teams = [
  team,
  { ...team, title: "Андрей", id: 2 },
  { ...team, title: "Воронеж", id: 3 },
  { ...team, title: "Метро", id: 4 },
  { ...team, title: "ЛПК", id: 5 },
];
