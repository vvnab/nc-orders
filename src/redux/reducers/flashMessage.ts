import { createSlice } from "@reduxjs/toolkit";

interface IInitialState {
  value: undefined | { type: "success" | "error"; message: string };
}

const initialState: IInitialState = { value: undefined };

export const flashMessage = createSlice({
  name: "flashMessage",
  initialState,
  reducers: {
    putFlashMessage: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const selectFlashMessage = (state: any) => state.flashMessage.value;
export const { putFlashMessage } = flashMessage.actions;

export const setFlashMessage = (value: any) => (dispatch: any) => {
  dispatch(putFlashMessage(undefined));
  dispatch(putFlashMessage(value));
};

export default flashMessage.reducer;
