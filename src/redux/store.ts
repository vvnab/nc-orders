import { configureStore } from "@reduxjs/toolkit";
import flashMessage from "./reducers/flashMessage";

export default configureStore({
  reducer: {
    flashMessage,
  },
});
