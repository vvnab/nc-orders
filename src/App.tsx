import React from "react";
import { Routes, Route, Outlet, Navigate } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import {
  setFlashMessage,
  selectFlashMessage,
} from "redux/reducers/flashMessage";

import NotFound from "screens/404";
import Home from "screens/Home";
import Login from "screens/Login";
import Teams from "screens/Teams";
import Flash from "components/common/Flash";

function PrivateOutlet() {
  const auth = true;
  return auth ? <Outlet /> : <Navigate to="/login" />;
}

export default function App() {
  const flash = useSelector(selectFlashMessage);
  const dispatch = useDispatch();

  return (
    <>
      <Routes>
        <Route path="login" element={<Login />} />
        <Route path="/" element={<PrivateOutlet />}>
          <Route index element={<Home />} />
          <Route path="/teams" element={<Teams />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
      {flash && (
        <Flash
          type={flash.type}
          message={flash.message}
          close={() => dispatch(setFlashMessage(undefined))}
        />
      )}
    </>
  );
}
