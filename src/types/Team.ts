import { IUser } from "./User";

export interface ITeam {
  id: number;
  title: string;
  leader: IUser;
  users: IUser[];
}
