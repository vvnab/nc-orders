import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faReply as faBack,
} from "@fortawesome/free-solid-svg-icons";

import Layout from "components/common/Layout";
import Button from "components/common/Button";
import Input from "components/common/Input";
import RadioButton from "components/common/RadioButton";

import { ITeam } from "types";

import { teams as teamsData } from "redux/mockdata";

import styles from "assets/styles/screens/Teams.module.scss";

function Teams() {
  const [teams, setTeams] = useState(teamsData);
  return (
    <Layout className={styles.wrap}>
      <div className={styles.top}>
        <div className={styles.header}>
          <Button type="borderless" href="/">
            <FontAwesomeIcon icon={faBack} />
          </Button>
          Изменение бригады
        </div>
        <div className={styles.title}>
          Мои бригады
          <Button type="borderless" href="/teams/new">
            <FontAwesomeIcon icon={faPlus} />
          </Button>
        </div>
        <Input
          placeholder="Поиск по названию/сотруднику"
          onChange={(e) => {
            setTeams(
              teamsData.filter(
                ({ title }) =>
                  title
                    .toUpperCase()
                    .indexOf(e.currentTarget.value.toUpperCase()) > -1
              )
            );
          }}
        />
      </div>
      <TeamList teams={teams} />
    </Layout>
  );
}

export default Teams;

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
  teams: ITeam[];
}
const TeamList = ({ teams, ...rest }: IProps) => {
  return (
    <div className={styles.scrollView} {...rest}>
      {teams.map(({ title, leader, users, id }) => (
        <div key={id} className={styles.team}>
          <div>
            <div className={styles.title}>{title}</div>
            <ul className={styles.users}>
              <li className={styles.leader}>
                {leader.lastName} {leader.firstName} (б)
              </li>
              {users.map(({ id, firstName, lastName }) => (
                <li key={id}>
                  {lastName} {firstName}
                </li>
              ))}
            </ul>
          </div>
          <RadioButton label="" defaultChecked/>
        </div>
      ))}
    </div>
  );
};
