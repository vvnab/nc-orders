import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSignOutAlt as faSignOut,
  faBookOpen as faBook,
} from "@fortawesome/free-solid-svg-icons";
import logo from "assets/icons/logo.svg";
import Layout from "components/common/Layout";
import Button from "components/common/Button";

import Team from "components/Team";
import Summary from "components/Summary";

import { team, summary } from "redux/mockdata";

import styles from "assets/styles/screens/Home.module.scss";

function Home() {
  return (
    <Layout className={styles.wrap}>
      <div className={styles.header}>
        <div className={styles.plate}>
          <img src={logo} alt="logo" className={styles.logo} />
          <div className={styles.username}>Петров А.В.</div>
        </div>
        <Button type="simple" className={styles.logout} href="/login">
          <FontAwesomeIcon icon={faSignOut} />
        </Button>
      </div>

      <Button href="/reports" className={styles.button}>
        <FontAwesomeIcon icon={faBook} />
        Отчёты
      </Button>

      <Team data={team} />
      <Summary data={summary} />
    </Layout>
  );
}

export default Home;
