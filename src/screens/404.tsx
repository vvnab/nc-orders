import React from "react";
import { useNavigate } from "react-router-dom";
import Layout from "components/common/Layout";
import logo from "assets/icons/logo.svg";

import styles from "assets/styles/screens/NotFound.module.scss";

function NotFound() {
  const navigate = useNavigate();
  return (
    <Layout className={styles.wrap}>
      <div
        className={styles.href}
        onClick={() =>
          window.history.length > 2 ? navigate(-1) : navigate("/")
        }
      >
        <img src={logo} alt="4" />
        <img src={logo} alt="0" className={styles.revert} />
        <img src={logo} alt="4" />
        <div>нет такой страницы, жамкните чтобы вернуться</div>
      </div>
    </Layout>
  );
}

export default NotFound;
