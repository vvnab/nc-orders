import React, { useState } from "react";

import { useFormik } from "formik";
import * as Yup from "yup";

import { useNavigate } from "react-router-dom";

import Layout from "components/common/Layout";
import Button from "components/common/Button";
import Input from "components/common/Input";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt } from "@fortawesome/free-solid-svg-icons";
import logo from "assets/icons/logo.svg";


import { useDispatch } from "react-redux";
import { setFlashMessage } from "redux/reducers/flashMessage";

import styles from "assets/styles/screens/Login.module.scss";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [disabled, setDisabled] = useState(false);
  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Обязательное поле"),
      password: Yup.string().required("Обязательное поле"),
    }),
    onSubmit: async (values, { resetForm }) => {
      setDisabled(true);
      try {
        // submit && (await submit(values));
        // setFlash &&
        //   setFlash({
        //     message: messages?.success,
        //     type: "success",
        //   });
        // resetForm();
        navigate('/');
        dispatch(setFlashMessage({ type: "success", message: "Войдено успешно" }));
      } catch (ex) {
        console.error(ex);
        dispatch(setFlashMessage({ type: "error", message: "Не войти" }));
        setDisabled(false);
      }
    },
  });
  return (
    <Layout className={styles.wrap} autoComplete="off">
      <div className={styles.header}>
        <img src={logo} alt="logo" className={styles.logo} />
        <h3>Вход в личный кабинет</h3>
      </div>
      <Input
        type="text"
        placeholder="Логин"
        {...formik.getFieldProps("name")}
        errored={formik.touched.name && !!formik.errors.name}
      />
      <Input
        type="password"
        placeholder="Пароль"
        {...formik.getFieldProps("password")}
        errored={formik.touched.password && !!formik.errors.password}
      />
      <div className={styles.plate}>
        <Button href="/reset" type="simple" className={styles.link}>
          Сбросить пароль
        </Button>
        <Button
          type="basic"
          disabled={disabled}
          action={() => formik.submitForm()}
        >
          <FontAwesomeIcon icon={faSignInAlt} /> Войти
        </Button>
      </div>
    </Layout>
  );
}

export default Login;
